# [STAGE 1] Build backend
FROM node:16-alpine As backend-development
WORKDIR /usr/src/app

# Install dependencies
COPY backend/package*.json ./
RUN npm install

# Build the app
COPY backend/. .
RUN npm run build


# [STAGE 2] Build frontend
FROM node:16-alpine As frontend-development
WORKDIR /usr/src/app

# Install dependencies
COPY frontend/package*.json ./
RUN npm install

# Build the app
COPY frontend/. .
RUN npm run build


# [Stage 3] Build production image
FROM node:16-alpine As production
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app

# Install dependencies
COPY backend/package*.json ./
RUN npm install --production

# COPY . .
# COPY backend/. .
COPY backend/example.json .

COPY --from=backend-development /usr/src/app/dist ./dist
COPY --from=frontend-development /usr/src/app/dist ./client

EXPOSE 80
CMD ["node", "dist/main"]