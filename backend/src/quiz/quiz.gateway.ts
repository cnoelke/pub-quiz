import { Logger } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { PlayerService } from './player.service';
import { AnswerDto, JoinQuizDto } from './quiz.dto';
import { Question } from './quiz.model';
import { QuizService } from './quiz.service';

const ANSWER_TIME_IN_SECONDS = 15;

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class QuizGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  private readonly server: Server;
  private readonly logger: Logger = new Logger(QuizGateway.name);

  constructor(
    private readonly playerService: PlayerService,
    private readonly quizService: QuizService,
  ) {}

  afterInit(): void {
    this.logger.debug('Initialize');
  }

  handleConnection(client: Socket): void {
    this.logger.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket): void {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('join-quiz')
  async joinQuiz(client: Socket, player: JoinQuizDto): Promise<void> {
    this.logger.debug(`Player joins quiz: ${player.nickname} (${client.id})`);
    this.playerService.addPlayer(player.identifier, player.nickname, client.id);
    this.server.emit('players', this.playerService.getPlayers());
  }

  @SubscribeMessage('next-question')
  async nextQuestion(): Promise<void> {
    this.logger.debug('Next question');
    const question = this.quizService.getNextQuestion();
    if (question) {
      await this.publishQuestion(question)
        .then((question) =>
          this.publishCountdown(ANSWER_TIME_IN_SECONDS, question),
        )
        .then((question) => this.publishCorrectAnswer(question))
        .then(() => this.quizService.closeRound())
        .then((pointsPerPlayer) => this.updateScores(pointsPerPlayer))
        .then(() => this.publishScores());
    } else {
      this.logger.debug('No questions left. End quiz.');
      this.publishQuizEnd();
    }
  }

  @SubscribeMessage('answer')
  async answer(client: Socket, answer: AnswerDto): Promise<void> {
    this.logger.debug(`Client answered: ${client.id}: ${answer}`);
    const player = this.playerService.findOneBySocketId(client.id);
    this.quizService.addAnswer(
      player.identifier,
      answer.questionId,
      answer.answer,
    );
    // TODO: end question if all players have answered
  }

  private publishQuizEnd() {
    this.playerService.clearScores();
    this.server.emit('quiz-end');
  }

  private async publishQuestion(question: Question): Promise<Question> {
    this.logger.debug(`Publish question ${question.identifier}`);
    const questionWithoutCorrectAnswer: Question = {
      ...question,
      acceptedAnswer: undefined,
    };
    this.server.emit('question', questionWithoutCorrectAnswer);
    return question;
  }

  private async publishCountdown<T>(seconds: number, value: T): Promise<T> {
    while (seconds >= 0) {
      this.server.emit('timer', seconds);
      await new Promise((resolve) => setTimeout(resolve, 1000));
      seconds--;
    }
    return value;
  }

  private publishCorrectAnswer(question: Question): Question {
    this.server.emit('correct-answer', question.acceptedAnswer);
    return question;
  }

  private publishScores(): void {
    this.server.emit('scores', this.playerService.getScores());
  }

  private updateScores(pointsPerPlayer: Record<string, number>): void {
    Object.entries(pointsPerPlayer).forEach(([identifier, points]) =>
      this.playerService.addScorePoints(identifier, points),
    );
  }
}
