# pub-quiz

TODO: description, tbd

## Getting started

TODO: tbd

## Architecture

TODO: general architecture (C4 component diagram), tbd

### Domain model

TODO: tbd

### API interface

TODO: routes, tbd

### Websocket interface

The clients (host browser, player browser) and the server send messages ofver the websocket protocol to communicate with each other.

#### Messages from player to server

| Event name  | Payload | Description |
| ----------- | ------- | ----------- |
| `join-quiz` | `{ identifier: string, nickname: string }` | Sent after the connection to server is established. Identifier and nickname are stored as cookies in the players browser, so in the case of a breakdown, the player can be identified (the socket id will change in this case). |
| `answer`    | `{ questionId: string, answer: string }` | Sent to answer the current question. |

#### Messages from host to server

| Event name      | Payload | Description |
| --------------- | ------- | ----------- |
| `next-question` | n/a | Sent to publish the next question. |

#### Messages from server to clients

| Event name       | Payload | Description |
| ---------------- | ------- | ----------- |
| `question`       | `{ identifier: string, text: string, eduQuestionType: 'Multiple choice' \| 'Open ended', suggestedAnswer: [{ text: string }] \| undefined }` | tbd |
| `quiz-end`       | n/a | tbd |
| `timer`          | `number` | tbd |
| `correct-answer` | `{ text: string }` | tbd |
| `scores`         | `[ { player: { identifier: string, name: string }, score: number } ]` | tbd |

### User interface

TODO: screenshots or wireframes
