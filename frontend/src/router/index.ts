import { createRouter, createWebHistory } from "vue-router";
import AppHost from "../views/AppHost.vue";
import AppHostOverview from "../views/AppHostOverview.vue";
import AppPlayer from "../views/AppPlayer.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", name: "player", component: AppPlayer },
    { path: "/host", name: "host-overview", component: AppHostOverview },
    { path: "/host/:quizId", name: "host", component: AppHost },
  ],
});

export default router;
