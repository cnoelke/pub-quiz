import { Injectable, Logger } from '@nestjs/common';
import { appendFile } from 'fs/promises';
import { PlayerService } from './player.service';

const MOUNTED_ANSWER_PATH = '/var/pub-quiz/quiz-answers.csv';
// eslint-disable-next-line prettier/prettier
const NEW_LINE = "\n";

@Injectable()
export class AuditService {
  private readonly logger: Logger = new Logger(AuditService.name);

  constructor(private readonly playerService: PlayerService) {
    this.logger.warn(AuditService.name);
  }

  async recordAnswers(
    questionId: string,
    answers: Record<string, string>,
  ): Promise<void> {
    const answersAsCsv = Object.entries(answers)
      .map(([identifier, answer]) => {
        const player = this.playerService.findOneByIdentifier(identifier);
        return player
          ? `${questionId},"${player.nickname}","${answer}"`
          : undefined;
      })
      .filter((value): value is string => !!value)
      .join(NEW_LINE);

    try {
      await appendFile(MOUNTED_ANSWER_PATH, answersAsCsv + NEW_LINE);
      this.logger.debug(answersAsCsv);
    } catch (e) {
      this.logger.debug(e);
    }
  }
}
