import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { existsSync, readFileSync } from 'fs';
import { resolve } from 'path';
import { AuditService } from './audit.service';
import { QuizInfoDto } from './quiz.dto';
import { Question, Quiz } from './quiz.model';

const MOUNTED_QUIZ_PATH = '/var/pub-quiz/quiz.json';

type QuizState = {
  currentQuestion: Question | undefined;
  answers: Record<string /* identifier */, string>;
};

@Injectable()
export class QuizService {
  private readonly logger: Logger = new Logger(QuizService.name);
  private readonly quiz: Quiz;
  private readonly quizState: QuizState;

  constructor(private readonly auditService: AuditService) {
    let quizFilePath = resolve(process.cwd(), 'example.json');

    if (existsSync(MOUNTED_QUIZ_PATH)) {
      quizFilePath = MOUNTED_QUIZ_PATH;
    } else {
      this.logger.warn(
        `No quiz provided at ${MOUNTED_QUIZ_PATH}. Using included example quiz.`,
      );
    }

    this.logger.debug(`Loading quiz from ${quizFilePath}`);
    this.quiz = JSON.parse(readFileSync(quizFilePath, 'utf-8'));
    this.logger.debug(`Loaded quiz with id ${this.quiz.identifier}`);
    this.quizState = {
      currentQuestion: undefined,
      answers: {},
    };
  }

  findAll(): QuizInfoDto[] {
    return [
      {
        identifier: this.quiz.identifier,
        name: this.quiz.name,
      },
    ];
  }

  findOne(id: string): QuizInfoDto {
    if (id === this.quiz.identifier) {
      return {
        identifier: this.quiz.identifier,
        name: this.quiz.name,
      };
    }
    throw new HttpException(
      `Quiz with id '${id}' does not exist.`,
      HttpStatus.BAD_REQUEST,
    );
  }

  reset(): void {
    this.logger.debug('Reset quiz');
    this.quizState.currentQuestion = undefined;
    this.quizState.answers = {};
  }

  // ----------

  getNextQuestion(): Question | undefined {
    this.quizState.answers = {};

    const indexOfCurrentQuestion = this.quiz.hasPart.indexOf(
      this.quizState.currentQuestion,
    );
    const nextQuestion = this.quiz.hasPart[indexOfCurrentQuestion + 1];
    this.quizState.currentQuestion = nextQuestion;

    return nextQuestion;
  }

  addAnswer(identifier: string, questionId: string, answer: string) {
    if (
      !this.quizState.currentQuestion ||
      this.quizState.currentQuestion.identifier !== questionId
    ) {
      return;
    }
    this.quizState.answers[identifier] = answer;
  }

  async closeRound(): Promise<Record<string, number>> {
    const currentQuestion = this.quizState.currentQuestion;
    const pointsPerPlayer: Record<string, number> = {};

    if (!currentQuestion) {
      return pointsPerPlayer;
    }

    if (currentQuestion.eduQuestionType === 'Multiple choice') {
      Object.entries(this.quizState.answers).forEach(([identifier, answer]) => {
        const points = currentQuestion.acceptedAnswer.text === answer ? 1 : 0;
        pointsPerPlayer[identifier] = points;
      });
    } else {
      const correctAnswerAsNumber = Number.parseInt(
        currentQuestion.acceptedAnswer.text,
      );
      Object.entries(this.quizState.answers)
        .map(([identifier, answer]): [string, number] => {
          const answerAsNumber =
            Number.parseInt(answer) ?? Number.MAX_SAFE_INTEGER;
          return [identifier, Math.abs(correctAnswerAsNumber - answerAsNumber)];
        })
        .sort(([, diffA], [, diffB]) => diffA - diffB)
        .forEach(([identifier], index) => {
          // closest 3 answer receive a point
          // TODO: if index >= 3 and answer equals answer of index = 2, index gets points as well
          const points = index < 3 ? 1 : 0;
          pointsPerPlayer[identifier] = points;
        });
    }

    await this.auditService.recordAnswers(
      currentQuestion.identifier,
      this.quizState.answers,
    );

    return pointsPerPlayer;
  }
}
