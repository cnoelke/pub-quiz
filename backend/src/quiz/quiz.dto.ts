export type QuizInfoDto = {
  identifier: string;
  name: string;
};

export type JoinQuizDto = {
  identifier: string;
  nickname: string;
};

export type AnswerDto = {
  questionId: string;
  answer: string;
};

export type ScoresDto = {
  player: { identifier: string; name: string };
  score: number;
}[];
