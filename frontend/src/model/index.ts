export type ScoresDto = {
  player: { identifier: string; name: string };
  score: number;
}[];

export type Answer = { text: string };

export type Question = {
  acceptedAnswer: Answer;
  eduQuestionType: "Multiple choice" | "Open ended";
  identifier: string;
  suggestedAnswer?: Answer[];
  text: string;
};

export type QuizInfoDto = {
  identifier: string;
  name: string;
};

export type JoinQuizDto = {
  identifier: string;
  nickname: string;
};

export type AnswerDto = {
  questionId: string;
  answer: string;
};
