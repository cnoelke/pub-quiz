import { Controller, Get, Param } from '@nestjs/common';
import { PlayerService } from './player.service';
import { QuizInfoDto } from './quiz.dto';
import { QuizService } from './quiz.service';

@Controller('quiz')
export class QuizController {
  constructor(
    private readonly quizService: QuizService,
    private readonly playerService: PlayerService,
  ) {}

  @Get()
  findAll(): QuizInfoDto[] {
    return this.quizService.findAll();
  }

  @Get('reset')
  reset(): void {
    this.playerService.reset();
    this.quizService.reset();
  }

  @Get(':quizId')
  findOne(@Param('quizId') quizId: string): QuizInfoDto {
    return this.quizService.findOne(quizId);
  }
}
