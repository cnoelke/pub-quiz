import { Module } from '@nestjs/common';
import { AuditService } from './audit.service';
import { PlayerService } from './player.service';
import { QuizController } from './quiz.controller';
import { QuizGateway } from './quiz.gateway';
import { QuizService } from './quiz.service';

@Module({
  providers: [AuditService, PlayerService, QuizGateway, QuizService],
  controllers: [QuizController],
})
export class QuizModule {}
