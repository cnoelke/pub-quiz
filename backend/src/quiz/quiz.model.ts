export type Answer = {
  '@context': 'https://schema.org';
  '@type': 'Answer';
  text: string;
  dateCreated?: Date;
  author?: {
    '@type': 'Person';
    identifier: string;
    name: string;
  };
};

export type Question = {
  '@context': 'https://schema.org';
  '@type': 'Question';
  identifier: string;
  // Creative Work
  text: string;
  // Question
  acceptedAnswer: Answer;
  eduQuestionType: 'Multiple choice' | 'Open ended';
  suggestedAnswer?: Answer[];
};

export type Quiz = {
  '@context': 'https://schema.org';
  '@type': 'Quiz';
  identifier: string;
  name: string;
  // Creative Work
  hasPart: Question[];
};
