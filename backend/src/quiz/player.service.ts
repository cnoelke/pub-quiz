import { Injectable } from '@nestjs/common';
import { ScoresDto } from './quiz.dto';

type Player = {
  identifier: string;
  nickname: string;
  score: number;
  socketId: string;
};

@Injectable()
export class PlayerService {
  // key: identifier
  private players: Record<string, Player> = {};

  addPlayer(identifier: string, nickname: string, socketId: string): void {
    if (this.players[identifier]) {
      this.players[identifier].nickname = nickname;
      this.players[identifier].socketId = socketId;
    } else {
      this.players[identifier] = {
        identifier,
        nickname,
        score: 0,
        socketId,
      };
    }
  }

  findOneByIdentifier(identifier: string): Player | undefined {
    return Object.values(this.players).find(
      (player) => player.identifier === identifier,
    );
  }

  findOneBySocketId(socketId: string): Player | undefined {
    return Object.values(this.players).find(
      (player) => player.socketId === socketId,
    );
  }

  addScorePoints(identifier: string, points: number): void {
    this.players[identifier].score += points;
  }

  clearScores(): void {
    Object.entries(this.players).forEach(([, player]) => (player.score = 0));
  }

  getScores(): ScoresDto {
    return Object.entries(this.players).map(([, player]) => ({
      player: { identifier: player.identifier, name: player.nickname },
      score: player.score,
    }));
  }

  getPlayers(): string[] {
    return Object.entries(this.players).map(([, player]) => player.nickname);
  }

  reset(): void {
    this.players = {};
  }
}
