// see https://dev.to/grawl/vue-3-composition-api-socketio-5den
import { io } from "socket.io-client";
import { WS_URL } from "../constant";

export const useSocketIO = () => {
  const socket = io(WS_URL);
  return { socket };
};
